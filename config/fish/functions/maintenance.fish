
# maintenance
function maintenance
    # purge all .cache files that have not been accessed in 30 days
    find ~/.cache/ -type f -atime +30 -delete
    # remove all but the most recent entries by size or time
    journalctl --vacuum-size=50M
    journalctl --vacuum-time=2weeks
    # remove any orphaned packages
    sudo pacman -Rs (pacman -Qdtq)
    # full mirror and package update
    pacman-mirrors --fasttrack && sudo pacman -Syyu
end
