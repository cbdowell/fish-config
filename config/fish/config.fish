# --------------------------------------------------------------------------- #
#  ___ _    _
# | __(_)__| |_
# | _|| (_-< ' \
# |_| |_/__/_||_|
#
# Author: Christopher Dowell <cbdowell@gmail.com>
# License: MIT
# --------------------------------------------------------------------------- #

# xdg
set -q XDG_CONFIG_HOME; or set -gx XDG_CONFIG_HOME "$HOME/.config"
set -q XDG_DATA_HOME;   or set -gx XDG_DATA_HOME   "$HOME/.local/share"
set -q XDG_CACHE_HOME;  or set -gx XDG_CACHE_HOME  "$HOME/.cache"

# locale
set -gx LC_ALL   en_US.UTF-8
set -gx LC_AL    en_US.UTF-8
set -gx LANG     en_US.UTF-8
set -gx TERM     xterm-256color
set -gx TERMINAL /usr/bin/termite
set -gx SHELL    /usr/bin/fish
set -gx PAGER    'less -X'
set -gx TMPDIR   /tmp
set -gx GPG_TTY  (tty)
set -gx BROWSER  /usr/bin/firefox

# editor
if type -q code
    set -q VISUAL || set -gx VISUAL code
    set -gx EDITOR code
else if type -q nano
    set -gx EDITOR nano
else
    set -gx EDITOR nvim
    set -gx VISUAL nvim
    alias vim nvim
end

# gtk
set -gx QT_QPA_PLATFORMTHEME        "qt5ct"
set -gx GTK2_RC_FILES               "$HOME/.gtkrc-2.0"
set -gx QT_AUTO_SCREEN_SCALE_FACTOR 0

# fzf
set -gx FZF_DEFAULT_COMMAND "fd --type f --hidden -E 'bundles/' -E '.git/'"
set -gx FZF_CTRL_T_COMMAND  "$FZF_DEFAULT_COMMAND"
set -gx FZF_ALT_C_COMMAND   "fd --type d . $HOME"
set -gx FZF_DEFAULT_OPTS    "--height 50% --layout=reverse --border"

# cheat
set -gx CHEAT_CONFIG_PATH "~/.config/cheat/conf.yml"

# ghq
set -gx GHQ_ROOT $HOME/.repos

# qutebrowser
set -x QUTEWAL_DYNAMIC_LOADING true

# bat
set -gx BAT_THEME "ansi-dark"
# set -gx MANPAGER "sh -c 'col -bx | bat -l man -p'"

set -gx GOPATH "$HOME/.go"

# paths
for binpath in $HOME/.node/bin $HOME/.cargo/bin $GOPATH/bin
    test -d $binpath
        and not contains $binpath $PATH
        and set fish_user_paths $binpath $fish_user_paths
end

# private
# if test -e $HOME/.dotfiles/extra.fish
#     source $HOME/.dotfiles/extra.fish
# end

# aliases
source ~/.config/fish/aliases.fish

# colors
eval (dircolors -c $HOME/.dir_colors 2> /dev/null)

# direnv
type -q direnv; and direnv hook fish | source

# starship
type -q starship; and starship init fish | source

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end

# eval (ssh-agent -c)
# ssh-add ~/.ssh/id_rsa
