function ..; cd ..; end
function ...; cd ../..; end
function ....; cd ../../..; end
function grep -d "File pattern searcher"
    command grep --color=auto $argv; end

function l -d "l"
    ls -lah $argv; end

function tree -d "List contents of directories in a tree-like format"
    command tree -C; end
function df -d "Display free disk space"
    command df -h $argv; end

function pwgen -d "Generate safe passwords"
    command pwgen --capitalize --numerals --secure 16; end

function axel -d "Multiple connections wget"
    command axel -an 10 $argv; end

function ssh-copy-id -d "Use locally available keys to authorise logins on a remote machine"
    env SSH_OPTS='-F /dev/null' command ssh-copy-id $argv; end

function ssht -d "Attach to remote tmux session"
    ssh $argv -t "tmux a"; end

function reload -d "Reload fish config"
    source ~/.config/fish/config.fish; end

function myip -d "Get public IP"
    dig +short myip.opendns.com @resolver1.opendns.com; end

function cleanup -d "Recursively delete .DS_Store"
    find . -type f -name '*.DS_Store' -ls -delete; end

function update -d "Update the system"
    sudo powerpill -Syu --noconfirm; end

function mpi -d "Install packages"
    sudo pacman -S $argv; end

function p8 -d "Ping 8.8.8.8"
    ping 8.8.8.8; end

function pg -d "Ping google.de"
    ping google.de; end

function ccat -d "Syntax highlighting cat"
    pygmentize -g $argv; end

function ssh_ignore -d "Ignore ssh config"
    ssh -F /dev/null $argv; end

function starwars -d "Play Star Wars in ASCII"
    telnet towel.blinkenlights.nl; end

function brewsync -d "Sync your installed tools"
    ansible-playbook $HOME/.dotfiles/ansible/dotfiles.yml --tags packages; end

function dotfiles -d "Run dotfiles Playbook"
    ansible-playbook $HOME/.dotfiles/ansible/dotfiles.yml $argv; end

function play -d "Run an ansible playbook (alias)"
    ansible-playbook $argv; end

alias du   'du -ch'
alias free 'free -m'
alias mk   'command mkdir -pv' # Create missing directories in path when calling `mkdir`
alias path 'readlink -e'       # `path` command to print full file path
alias rmm  'rm -rvI'           # `rmm` command to remove directories, but ask nicely
alias cpp  'cp -R'             # `cpp` command to copy directories, but ask nicely
alias c    'clear'

# Gota go..
alias :q exit
alias :qa exit

command -q -s 'hub' ; and alias git 'hub'
command -q -s 'tldr'; and alias man 'tldr'
command -q -s 'bat' ; and alias cat 'bat'

# ls
command -q -s 'exa'
    and alias ls 'ls --color'
    and alias ll 'exa -lh'
    and alias la 'exa -alh'

# Command to add current directory to path
alias add-to-path 'set -U fish_user_paths (pwd) $fish_user_paths'

# Update `PATH` variable
alias path-update 'set -gx PATH (bash -c "source ~/git/stuff/config/path; echo \$PATH")'

#  * Human readable sizes for `df`, `du`, `free` (i.e. Mb, Gb etc)
alias df   'df -h'
alias du   'du -ch'
alias free 'free -m'

#  * `fs` command to show free space on physical drives
alias fs 'df -h -x squashfs -x tmpfs -x devtmpfs'

#  * `disks` command to List disks
#    - Clearly shows which disks are mounted temporary
#    - I always run this command before `dd` sd-card, to make 100% sure not to override system partition
alias disks 'lsblk -o HOTPLUG,NAME,SIZE,MODEL,TYPE | awk "NR == 1 || /disk/"'

#  * `partitions` command to list partitions
alias partitions 'lsblk -o HOTPLUG,NAME,LABEL,MOUNTPOINT,SIZE,MODEL,PARTLABEL,TYPE,UUID | grep -v loop | cut -c1-$COLUMNS'

#  * `sizeof` command to show size of file or directory
alias sizeof "du -hs"

#  * `connect` command Connect to wifi from terminal
alias connect nmtui

#  * `lockblock` command to prevent screen locking untill next reboot
alias lockblock 'killall xautolock; xset s off; xset -dpms; echo ok'

#  * `wget` to save file with provided name
# alias wget ='wget --content-disposition'

#  * Show 3 (next and prev) months in cal, start week on monday
#    - Use [nicl](https://github.com/dmi3/nicl) in installed
if type -q nicl
    alias cal "nicl -w3 -f ~/git/stuff/documents/bank_days.csv"
else
    alias cal "ncal -bM3"
end

#  * If [sssh2](https://github.com/dmi3/bin/blob/master/sssh2) installed - use it instead of ssh
if type -q sssh2
    alias ssh sssh2
end

#  * Show images in [kitty](https://sw.kovidgoyal.net/kitty/)
if type -q kitty
    alias icat "kitty +kitten icat"
end

alias colortest  "curl -s https://gist.githubusercontent.com/HaleTom/89ffe32783f89f403bba96bd7bcd1263/raw/ | bash"
alias myip       "curl cip.cc"
alias br         "xrandr --output eDP1 --brightness"
alias open       "xdg-open"
alias loginmysql "mysql -u root -p"
alias clipboard  "xclip -selection clipboard"


if status --is-interactive
    set -g fish_user_abbreviations

    # abbr --add k kubectl
    # abbr --add c code
    # abbr --add g git

    # get weather
    abbr -ag get-weather "curl wttr.in"
    abbr -ag getweather "curl wttr.in"
end

# colors
alias colors "$HOME/.config/base16-shell/colortest"

function pacfzf
    pacman -Qq | fzf --preview 'pacman -Qil {}' --layout=reverse --bind 'enter:execute(pacman -Qil {} | less)'
end
